//
//  LevelOneScene.m
//  Time Towers
//
//  Created by Ryan Salton on 16/02/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "LevelOneScene.h"
#import "TTPathNode.h"


@interface LevelOneScene ()

@property (nonatomic, assign, getter = isInPauseState) BOOL inPauseState;
@property (nonatomic, strong) NSMutableArray *wavesArray;
@property (nonatomic, assign) NSInteger currentWave;

@property (nonatomic, strong) TTPathNode *pathNode;

@end


@implementation LevelOneScene

-(void)didMoveToView:(SKView *)view {
    /* Setup your scene here */
    self.backgroundColor = [UIColor greenColor];
    
//    self.wavesArray = [NSMutableArray array];
//    
//    [self.wavesArray addObject:@[]];
    
    [self createPaths];
    
    self.currentWave = 0;
    self.inPauseState = YES;
}

- (void)createPaths
{
    UIBezierPath *trackPath = [UIBezierPath bezierPath];
    [trackPath moveToPoint:CGPointMake(self.size.width * 0.5, self.size.height)];
    [trackPath addCurveToPoint:CGPointMake(150, 500)
                 controlPoint1:CGPointMake(220, 450)
                 controlPoint2:CGPointMake(100, 550)];
    [trackPath addCurveToPoint:CGPointMake(self.size.width, 100)
                 controlPoint1:CGPointMake(300, 200)
                 controlPoint2:CGPointMake(200, 480)];
    
    self.pathNode = [TTPathNode shapeNodeWithPath:trackPath.CGPath];
    [self addChild:self.pathNode];
}

- (void)startWave
{
    SKSpriteNode *enemy = [SKSpriteNode spriteNodeWithColor:[UIColor redColor] size:CGSizeMake(20, 20)];
    enemy.position =
}

- (NSMutableArray *)pathsArray
{
    if (!_pathsArray)
    {
        _pathsArray = [NSMutableArray array];
    }
    
    return _pathsArray;
}

- (NSMutableArray *)enemiesArray
{
    if (!_enemiesArray)
    {
        _enemiesArray = [NSMutableArray array];
    }
    
    return _enemiesArray;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        
        if (self.isInPauseState)
        {
            [self startWave];
        }
    }
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
}

@end
