//
//  LevelOneScene.h
//  Time Towers
//
//  Created by Ryan Salton on 16/02/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface LevelOneScene : SKScene

@property (nonatomic, strong) NSMutableArray *pathsArray;
@property (nonatomic, strong) NSMutableArray *enemiesArray;

@end
