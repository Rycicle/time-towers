//
//  GameViewController.m
//  Time Towers
//
//  Created by Ryan Salton on 16/02/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "GameViewController.h"
#import "LevelOneScene.h"


@interface GameViewController ()

@property (nonatomic, strong) SKView *skView;

@end


@implementation GameViewController

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    if (!self.skView)
    {
        // Configure the view.
        self.skView = (SKView *)self.view;
        self.skView.showsFPS = YES;
        self.skView.showsNodeCount = YES;
        /* Sprite Kit applies additional optimizations to improve rendering performance */
        self.skView.ignoresSiblingOrder = YES;
        
        // Create and configure the scene.
        LevelOneScene *scene = [[LevelOneScene alloc] initWithSize:self.view.frame.size];
        scene.scaleMode = SKSceneScaleModeAspectFill;
        
        // Present the scene.
        [self.skView presentScene:scene];
    }
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
