//
//  TTPathNode.m
//  Time Towers
//
//  Created by Ryan Salton on 16/02/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "TTPathNode.h"

@implementation TTPathNode

- (instancetype)initWithPoints:(NSArray *)pointsArray
{
    self = [super init];
    
    if (self)
    {
        self.pointsArray = [NSMutableArray arrayWithArray:pointsArray];
        
        self.strokeColor = [UIColor yellowColor];
        self.lineWidth = 10;
    }
    
    return self;
}

@end
